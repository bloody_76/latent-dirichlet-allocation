function [ alpha, beta ] = parameter_estimation( gam, phi, words )
%% Optimize the parameters alpha and beta considering the total logLikelihood.
% Things to know:
%   - M = number of documents ;
%   - K = number of topics ;
%   - N = number of words ;
%   - V = size of vocabulary.

    % We get the number of documents.
    [M,K] = size(gam);          % gam is of type MxK.
    V = size(words{1}, 2);      % words is of type Cell(M,1)xNdxV.
    
    % Init.
    beta = zeros(K,V);
   
    % Iterate over all the matrix and construct beta matrix.
    for d = 1 : M
        beta = beta + phi{d}' * words{d};
    end
    % We normalize beta for each topic.
    for i = 1 : K
        beta(i,:) = beta(i,:) ./ norm(beta(i,:),1);
    end
        
    % Update alpha.
    alpha = newton_raphson(gam);
    alpha = alpha ./ norm(alpha,2);
end

