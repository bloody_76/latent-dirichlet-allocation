function [] = display_phis( phi, words, vocab, files )

    D = size(phi,1);
    
    figure;
    hold on;
    
    for i = 1 : D
        subplot(D,1,i);
        bar(words{i}' * phi{i});
        title(files(i));
        set(gca, 'XTickLabel', vocab);
    end

end

