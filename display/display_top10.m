function [ ] = display_top10( beta, vocab )
% display_top10 will display the 10 best representative words over topics.

    K = size(beta,1);
    
    for i = 1 : K
        bests = beta(i,:);
        [bests,I] = sort(bests, 'descend');
        bests = bests(1:min(10,size(beta,2)));
        
        subplot(K,1,i);
        bar(bests);
        set(gca, 'XTickLabel', vocab(I,:));
    end

end

