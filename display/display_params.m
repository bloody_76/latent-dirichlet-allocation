function [ ] = display_params( alpha, beta, gamma, vocab, files )

    figure;
    hold on;
    subplot(3,1,1);
    bar(alpha);
    title('Alpha, Dirichlet parameter');
    
    subplot(3,1,2);
    bar(beta');
    title('Beta: proportion of words over topics');
    set(gca, 'XTickLabel', vocab);
    
    subplot(3,1,3);
    bar(gamma);
    title('Gamma: proportion of topics over documents');
    set(gca, 'XTickLabel', files);
    

end

