function [ l ] = logL( g, p, a, b, w )
%% Compute the loglikelihood of a single document.
% Version: 0.1 (not optimized)
%
% Things to know:
%   - K = number of topics.
%   - N = number of words ;
%   - V = size of vocabulary.
%
% Properties:
%   - words: each row is a bit-vector of size V where sum(a row) = 1, and
%   the i-th bit in the vector which is equal to 1 says the word is the
%   i-th in the vocabulary.
%
% Parameters:
%   - g: gamma, matrix of K elts ;
%   - p: phi, matrix of NxK elts ;
%   - a: alpha, matrix of K elts ;
%   - b: beta, matrix of KxV elts ;
%   - w: words, matrix of NxV elts.

    N = size(p, 1);
    K = size(g, 1);

    l = log(gamma(sum(a))) - sum(log(gamma(a))) + sum((a - 1) .* (psi(g) - psi(sum(g))));
    
    l = l + sum(sum(p .* repmat((psi(g) - psi(sum(g))),1,N)'));
    
    %l = l + sum(sum(sum(p * w * log(b))));
    %         n   i   j
    for n = 1 : N
        for i = 1 : K
            % Recall there is only one bit to 1 in w(n,:) (c.f. Properties).
            j = w(n,:) == 1;
            l = l + p(n,i) * w(n,j) * log(b(i,j));
        end
   end
    l = l - log(gamma(sum(g))) + sum(log(gamma(g))) - sum((g - 1) .* (psi(g) - psi(sum(g))));
    %l = l - sum(sum(p * log (p)));
    %         n   i
    for n = 1 : N
        for i = 1 : K
            l = l - p(n,i) * log(p(n,i));
        end
    end
end