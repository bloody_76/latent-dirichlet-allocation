function [ alpha ] = newton_raphson( gammas, ini_alpha )
%% Compute the newton & raphson method.

    [M,K] = size(gammas);

    if ~(M > 1)
      alpha = gammas(1,:)';
      return;
    end
    
    % Mean of proportion of topics in the the documents.
    if nargin < 2 % Check the number of arguments.
        ini_alpha = mean(gammas)' / K; % initial point
    end
    
    maxiter = 20;
    alpha=ini_alpha;
    palpha=zeros(K,1);
    
    for t = 1 : maxiter
        % We calculate f.
        f = M * (psi(sum(alpha)) - psi(alpha)) + sum(psi(gammas),1)' - sum(psi(sum(gammas,2)),1);
        % We calculate df.
        df = M * psi(1,alpha) - psi(1,sum(alpha));
        
        % We want to maximize alpha.
        alpha = alpha + f ./ df;
        
        % If any alpha parameter is negative, we try again.
        % There is no sense of a negative parameter alpha for the Dirichlet
        % distribution.
        if any(alpha < 0)
            alpha = newton_raphson(gammas, ini_alpha / 10);
            return;
        end
        
        % Test for the convergence if we have already done an iteration.
        if (t > 2 && norm(alpha - palpha) / norm(alpha) < 10e-2)
            break;
        end
        palpha = alpha;
    end
end

