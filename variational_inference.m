function [ phi, gam ] = variational_inference( alpha, beta, w )
%% Optimize the free varational parameters phi and gamma considering the logLikelihood of the document.
% Things to know:
%   - K = number of topics ;
%   - N = number of words.
%
% Properties:
%   - words: each row is a bit-vector of size V where sum(a row) = 1, and
%   the i-th bit in the vector which is equal to 1 says the word is the
%   i-th in the vocabulary.
%
%   - phi: sum(n,:) = 1.

    K = size(alpha, 1);
    N = size(w, 1);

    % Init phi and gamma vectors.
    gam = alpha + N / K;
    phi = ones(N,K) / K;
    
    l = 1;
    old_l = -Inf;
    psi_sum_g = psi(sum(gam));
    % Loop until convergence.
    while (norm(l - old_l) / norm(l) > 10e-3)
        % Variational inference.
        phi = (w * beta') .* repmat((exp(psi(gam) - psi_sum_g))',N,1);
        for n = 1 : N
            % Normalize phi to sum to 1.
            phi(n,:) = phi(n,:) / norm(phi(n,:), 1);
        end
        % gamma = alpha + sum over n of phi.
        gam = alpha + sum(phi,1)';
        gam = gam / norm(gam);
        
        % Update our convergence considering the logLikelihood.
        old_l = l;
        l = logL(gam, phi, alpha, beta, w);
    end

end

