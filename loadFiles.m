function [ files,words,vocab ] = loadFiles( corpus_dir )


if nargin < 2
    corpus_dir = 'corpus';
end

dirData = dir(corpus_dir);      %# Get the data for the current directory
dirIndex = [dirData.isdir];  %# Find the index for directories
files = {dirData(~dirIndex).name};  %'# Get a list of the files

[words, vocab] = file2doc(files);

end

