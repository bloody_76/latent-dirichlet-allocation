function [ alpha, beta, gam, phi ] = lda( words, K, max_iter )
%% Apply the Latent Dirichlet Allocation model on the corpus of documents.
% M = number of documents.
% K = number of topics.
% N = number of words.

    M = size(words, 1);
    V = size(words{1}, 2);
    
    % We first need to create vectors from our text corpus.
    phi = cell(M,1);
    gam = zeros(M,K);

    % Initialize alpha and beta.
    beta = rand(K,V);
    for i = 1 : K
        beta(i,:) = beta(i,:) / norm(beta(i,:),1);
    end
    alpha = sort(rand(K,1), 'descend');
    alpha = alpha / norm(alpha,1);

    t_conv = 1;
    old_t_conv = -Inf;
    iter = 0;
    % We iterate until convergence.
    while (norm(t_conv - old_t_conv) / norm(t_conv) > 10e-4 && iter < max_iter)
        % E-Step : Variational inference, for each document.
        for d = 1 : M
            [phi{d}, gam(d,:)] = variational_inference(alpha, beta, words{d});
        end

        % M-Step : Parameter estimation.
        [alpha, beta] = parameter_estimation(gam, phi, words);

        % Convergence update.
        iter = iter + 1
        old_t_conv = t_conv;
        t_conv = total_logL(gam, phi, alpha, beta, words);
    end
end
