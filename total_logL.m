function [ l ] = total_logL( g, p, a, b, w )
%% Compute the total logLikelihood of a corpus of documents.
% Things to know:
%   - M = number of documents ;
%   - K = number of topics ;
%   - N = number of words.
%   - V = size of vocabulary.
%
% For the total logLikelihood we didn't specify the document so:
%   - g = MxK ;
%   - p = MxNxK ;
%   - a = K ;
%   - b = KxV ;
%   - w = MxNxV.

    M = size(g, 1);
    l = 0;
    
    for d = 1 : M
        l = l + logL(g(d,:)', p{d}, a, b, w{d});
    end

end

