function [ words, vocab ] = file2doc( file_list )

    vocabulary = containers.Map();
    size_d = containers.Map('KeyType', 'uint32', 'ValueType', 'uint32');
    vocab = [];
    % We first look into all the files to set the all vocabulary.
    for i = 1 : size(file_list, 2)
        fd = fopen(file_list{i});
        length = 0;
        
        word = fgetl(fd);
        while (ischar(word))
            if (vocabulary.isKey(word) == 0)
                vocabulary(word) = vocabulary.length() + 1;
                
                if size(vocab,1) == 0
                    vocab = word;
                else
                    vocab = char(vocab, word);
                end
            end
            word = fgetl(fd);
            length = length + 1;
        end
        
        size_d(i) = length;
        fclose(fd);
    end
    
    words = cell(size(file_list, 2), 1);
    % Then we can build our words.
    for i = 1 : size(file_list, 2)
        fd = fopen(file_list{i});
        words{i} = zeros(size_d(i), vocabulary.length());
        iter = 1;
        
        word = fgetl(fd);
        while ischar(word)
            words{i}(iter, vocabulary(word)) = 1;
            word = fgetl(fd);
            iter = iter + 1;
        end
        
        fclose(fd);
    end
end

